#!/usr/bin/env node


'use strict';

var _index = require('../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var args = process.argv.slice(2);

if (~args.indexOf('kill')) _index2.default.kill();else if (args.length) _index2.default.start.apply(_index2.default, _toConsumableArray(args.slice(1)));else console.log('beep');
