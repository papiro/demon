'use strict'

import cp from 'child_process'
import fs from 'fs'
import path from 'path'

export default {
  start(scriptPath, args = [], options = { silent: true }, callback = () => {}){
    if(typeof scriptPath !== 'string') return callback(new Error('scriptPath is undefined'))
    if(typeof args === 'function') {
      callback = args
      args = []
    } else if(typeof options === 'function') {
      callback = options
      options = {}
    }

    let _process = cp.fork(path.resolve(path.dirname(module.parent.filename), scriptPath), args, options)

    fs.appendFile(`${process.env.HOME}/.demontmp`, `${_process.pid}\n`, { encoding : 'utf8' }, (err) => callback(err, _process))
  },
  kill(callback = () => {}){
    fs.readFile(`${process.env.HOME}/.demontmp`, 'utf8', (err, data) => {
      if(err)
        if(err.code === 'ENOENT') return callback(new ReferenceError('demon not currently tracking any processes'))
        else
          return callback(err)
      
      cp.exec(`kill ${data.split('\n').join(' ')}`, (err, stdout, stderr) => {
        let callbackPayload = [[err], stdout, stderr]
        fs.unlink(`${process.env.HOME}/.demontmp`, (err) => {
          callbackPayload[0].push(err)
          callback(...callbackPayload)
        }) 
      })
    })
  }
}
