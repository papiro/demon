#!/usr/bin/env node

'use strict'

import demon from '../index'

let args = process.argv.slice(2)

if( ~args.indexOf('kill') )
  demon.kill()
else if( args.length )
  demon.start(...args.slice(1))
else
  console.log('beep')
