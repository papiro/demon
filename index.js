'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _child_process = require('child_process');

var _child_process2 = _interopRequireDefault(_child_process);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  start: function start(scriptPath) {
    var args = arguments.length <= 1 || arguments[1] === undefined ? [] : arguments[1];
    var options = arguments.length <= 2 || arguments[2] === undefined ? { silent: true } : arguments[2];
    var callback = arguments.length <= 3 || arguments[3] === undefined ? function () {} : arguments[3];

    if (typeof scriptPath !== 'string') return callback(new Error('scriptPath is undefined'));
    if (typeof args === 'function') {
      callback = args;
      args = [];
    } else if (typeof options === 'function') {
      callback = options;
      options = {};
    }

    var _process = _child_process2.default.fork(_path2.default.resolve(_path2.default.dirname(module.parent.filename), scriptPath), args, options);

    _fs2.default.appendFile(process.env.HOME + '/.demontmp', _process.pid + '\n', { encoding: 'utf8' }, function (err) {
      return callback(err, _process);
    });
  },
  kill: function kill() {
    var callback = arguments.length <= 0 || arguments[0] === undefined ? function () {} : arguments[0];

    _fs2.default.readFile(process.env.HOME + '/.demontmp', 'utf8', function (err, data) {
      if (err) if (err.code === 'ENOENT') return callback(new ReferenceError('demon not currently tracking any processes'));else return callback(err);

      _child_process2.default.exec('kill ' + data.split('\n').join(' '), function (err, stdout, stderr) {
        var callbackPayload = [[err], stdout, stderr];
        _fs2.default.unlink(process.env.HOME + '/.demontmp', function (err) {
          callbackPayload[0].push(err);
          callback.apply(undefined, callbackPayload);
        });
      });
    });
  }
};
