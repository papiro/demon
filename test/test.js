'use strict'

import cp from 'child_process'
import fs from 'fs'

import { assert } from 'chai'

import demon from '../index'

describe('demon as a cli utility', () => {
  let pid = 0
  it('should be available as the command "demon"', (done) => {
    cp.exec('command -v demon', (err, stdout, stderr) => {
      assert(!err, 'there was no error')
      done()
    })
  })
  it('should output "beep" if no commands are given', (done) => {
    cp.exec('demon', (err, stdout, stderr) => {
      assert.include(stdout, 'beep', 'outputs "beep" to stdout')
      done()
    })
  })
  describe('demon start', () => {
    it('should take a path to a javascript and run the script as a child process)', (done) => {
      cp.exec('demon start js/example.js', (err, stdout, stderr) => {
        assert(!err, 'there was no error')
        done()
      })
    })
    it('should keep track of the child process\'s pid', (done) => {
      fs.readFile(`${process.env.HOME}/.demontmp`, 'utf8', (err, data) => {
        assert(!err, 'there was no error')
        assert.match(data, /\d*/, 'contains a pid')
        done()
      })
    })
  }) 
  describe('demon kill', () => {
    it('should kill all currently tracked processes', (done) => {
      cp.exec('demon kill', (err, stdout, stderr) => {
        assert(!err, 'there was no error')
        done()
      })
    })
    it('should delete ~/.demontmp', (done) => {
      fs.readFile(`${process.env.HOME}/.demontmp`, 'utf8', (err, data) => {
        assert.strictEqual(err.code, 'ENOENT', '.demontmp was deleted')
        done()
      })
    })
  })
})
describe('demon as a node module', () => {
  let pid = 0
  it('should contain two methods: kill() and start()', () => {
    assert.property(demon, 'start', 'has start')
    assert.property(demon, 'kill', 'has kill')
  })
  describe('demon.start()', () => {
    it('should take a path to a javascript and run the script as a child process)', (done) => {
      demon.start('js/example.js', (err, childProcess) => {
        assert(!err, 'there was no error')
        assert.isNumber(pid, 'pid is a number') 
        pid = childProcess.pid  
        done()
      }) 
    })
    it('should show a process running at the pid returned in the callback', (done) => {
      cp.exec(`ps -p ${pid}`, (err, stdout, stderr) => {
        assert(!err, 'there was no error')
        assert.include(stdout, pid, 'a lookup of the pid contains a process')
        done()
      })
    })
    it('should keep track of the child process\'s pid', (done) => {
      fs.readFile(`${process.env.HOME}/.demontmp`, 'utf8', (err, data) => {
        assert(!err, 'there was no error')
        assert.include(data, pid, 'contains the pid')
        done()
      })
    })
  }) 
  describe('demon.kill()', () => {
    it('should kill all currently tracked processes', (done) => {
      demon.kill(err => {
        assert.sameMembers(err, [null, null], 'there were no errors')        
        cp.exec(`ps -p ${pid}`, (err, stdout, stderr) => {
          assert.ok(err, 'there was no error')
          assert.notInclude(stdout, pid, 'a lookup of the pid contains nothing')
          done()
        })
      })
    })
    it('should delete ~/.demontmp', (done) => {
      fs.readFile(`${process.env.HOME}/.demontmp`, 'utf8', (err, data) => {
        assert.strictEqual(err.code, 'ENOENT', '.demontmp was deleted')
        done()
      })
    })
    it('should return with an error if kill is called but .demontmp doesn\'t exist', (done) => {
      demon.kill(err => {
        assert.strictEqual(err.message, 'demon not currently tracking any processes', '.demontmp doesn\'t exist')
        done()
      })
    })
  })
})
