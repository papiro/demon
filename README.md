# demon

## demon is for running javascripts in node in the background on linux machines

Usage:

```
sudo npm install -g demon
```
From anywhere:
```
: demon path/to/script.js
Node process running on 11116
: cat ~/.demontmp
11116
```

.demontmp keeps track of the processes begun by demon.
use:
```
demon kill
```
to kill all processes begun by demon and to remove ~/.demontmp

### Note: demon intentionally runs the risk of a process ending naturally and the pid getting recycled.  The user should be the failsafe against blithely running `demon kill`.

```
let demon = require('demon')

demon.start('path/to/my/javascript', [some, args, for, child_process.fork], { some : options, for : it }, (err, childProcess) => {
  if(err) console.log('it')
  else console.log(childProcess.pid)
})
demon.kill()  
demon.start('nother/one') <-- also works
demon.start('another', callback) <--also works
```
